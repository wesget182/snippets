"use client";
import { redirect } from "next/navigation";
import { db } from "@/db";
import { createSnippet } from "@/actions";
import { useFormState } from "react-dom";
export default function SnippetCreatePage() {
	const [formState, action] = useFormState(createSnippet, {
		message: ""
	});

	return (
		<form action={action}>
			<h3 className="font-bold m-3">Create A Snippet</h3>
			<div className="flex flex-col gap-4">
				<div className="flex gap-4">
					<label className="w-12" htmlFor="title">
						Title
					</label>
					<input
						name="title"
						className="border text-black rounded p-2 w-full"
						id="title"
					/>
				</div>
				<div className="flex gap-4">
					<label className="w-12" htmlFor="code">
						Code
					</label>
					<textarea
						name="code"
						className="border text-black rounded p-2 w-full"
						id="code"
					/>
				</div>
				{formState.message ? (
					<div className="my-2 p-2 bg-red-200 border rounded border-red-400 text-black">
						{formState.message}
					</div>
				) : null}
				<button
					type="submit"
					className="rounded p-2 text-black font-bold bg-blue-200"
				>
					Create
				</button>
			</div>
		</form>
	);
}
